FROM eclipse-temurin:21
EXPOSE 8080
RUN mkdir /reactive-coroutines
WORKDIR /reactive-coroutines
COPY build/libs/reactive-coroutines.jar /reactive-coroutines/reactive-coroutines.jar
RUN chown -R 1000 /reactive-coroutines
CMD java ${JAVA_OPTS} -jar /reactive-coroutines/reactive-coroutines.jar
