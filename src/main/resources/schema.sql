CREATE TABLE IF NOT EXISTS calculation_history(
    id SERIAL PRIMARY KEY,
    operator TEXT NOT NULL,
    operand1 INTEGER NOT NULL,
    operand2 INTEGER NOT NULL,
    result INTEGER NOT NULL,
    is_even BOOLEAN NOT NULL
);
