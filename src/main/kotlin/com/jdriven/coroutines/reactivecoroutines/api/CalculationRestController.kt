package com.jdriven.coroutines.reactivecoroutines.api

import com.jdriven.coroutines.reactivecoroutines.domain.CalculationHistory
import com.jdriven.coroutines.reactivecoroutines.domain.CalculationRequest
import com.jdriven.coroutines.reactivecoroutines.domain.CalculationResult
import com.jdriven.coroutines.reactivecoroutines.domain.Operator
import com.jdriven.coroutines.reactivecoroutines.services.CalculationHistoryService
import com.jdriven.coroutines.reactivecoroutines.services.CalculationService
import kotlinx.coroutines.flow.Flow
import mu.KLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/calculation")
class CalculationRestController(
    private val calculationService: CalculationService,
    private val calculationHistoryService: CalculationHistoryService,
) {

    @GetMapping("/{operator}/{operand1}/{operand2}")
    suspend fun calculate(@PathVariable operator: String,
                          @PathVariable operand1: Int,
                          @PathVariable operand2: Int): CalculationResult {
        val request = CalculationRequest(
            operator = Operator.valueOf(operator.uppercase()),
            operand1 = operand1,
            operand2 = operand2,
        )
        logger.info { "Received calculation request $request..." }

        return calculationService.calculate(request = request).also {
            logger.info { "Returning response for request $request, result: $it" }
        }
    }

    @GetMapping("/history")
    fun findHistory(): Flow<CalculationHistory> = calculationHistoryService.findHistory()

    @GetMapping("/history/{id}")
    suspend fun findHistoryById(@PathVariable id: Long): CalculationHistory? =
        calculationHistoryService.findHistoryById(id)

    companion object: KLogging()
}
