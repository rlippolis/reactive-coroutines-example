package com.jdriven.coroutines.reactivecoroutines.services

import com.jdriven.coroutines.reactivecoroutines.domain.CalculationRequest
import com.jdriven.coroutines.reactivecoroutines.domain.CalculationResult
import com.jdriven.coroutines.reactivecoroutines.domain.Operator
import mu.KLogging
import org.springframework.stereotype.Service

@Service
class CalculationService(
    private val numberIsEvenService: NumberIsEvenService,
    private val calculationHistoryService: CalculationHistoryService,
) {

    /**
     * Performs the calculation, determines if the result is even using the `NumberIsEvenService`,
     * and adds the calculation to the history. Everything is performed without blocking (but suspending instead).
     */
    suspend fun calculate(request: CalculationRequest): CalculationResult {
        val result = doCalculation(request)
        val resultIsEven = numberIsEvenService.isEven(result)

        val calculationResult = CalculationResult(result = result, isEven = resultIsEven)
        logger.info { "Calculated result for request $request: $result" }

        calculationHistoryService.addToHistory(request = request, result = calculationResult)

        logger.info { "Returning result for request $request: $result" }
        return calculationResult
    }

    private fun doCalculation(request: CalculationRequest) = when (request.operator) {
        Operator.ADD      -> request.operand1 + request.operand2
        Operator.SUBTRACT -> request.operand1 - request.operand2
        Operator.MULTIPLY -> request.operand1 * request.operand2
        Operator.DIVIDE   -> request.operand1 / request.operand2
    }

    companion object: KLogging()
}
