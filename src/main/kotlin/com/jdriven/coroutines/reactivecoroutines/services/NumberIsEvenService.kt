package com.jdriven.coroutines.reactivecoroutines.services

import kotlinx.coroutines.reactor.awaitSingle
import mu.KLogging
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

/**
 * This service uses the isEven API (https://isevenapi.xyz/) to determine if a number is even.
 */
@Service
class NumberIsEvenService(
    webClientBuilder: WebClient.Builder,
) {
    // `WebClient` is the reactive variant of the Spring Web `RestTemplate`.
    private val webClient = webClientBuilder
        .baseUrl("https://api.isevenapi.xyz/api/iseven/")
        .build()

    suspend fun isEven(number: Int): Boolean {
        check(number in 0..999_999) // The isEven API only supports this range

        // Calls the isEven REST API without blocking (but suspending on the `awaitSingle()` function call)
        logger.info { "Calling the isEven API with number: $number" }
        val result = webClient.get()
            .uri("{number}", number)
            .retrieve()
            .bodyToMono(IsEvenResult::class.java)
            .awaitSingle()
            .iseven

        logger.info { "Returning result for number $number: isEven? $result" }
        return result
    }

    companion object: KLogging()
}

private data class IsEvenResult(val iseven: Boolean)
