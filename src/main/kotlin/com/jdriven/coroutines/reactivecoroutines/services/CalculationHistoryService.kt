package com.jdriven.coroutines.reactivecoroutines.services

import com.jdriven.coroutines.reactivecoroutines.domain.CalculationHistory
import com.jdriven.coroutines.reactivecoroutines.domain.CalculationRequest
import com.jdriven.coroutines.reactivecoroutines.domain.CalculationResult
import com.jdriven.coroutines.reactivecoroutines.repositories.CalculationHistoryRepository
import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import mu.KLogging
import org.springframework.stereotype.Service

@Service
class CalculationHistoryService(
    private val calculationHistoryRepository: CalculationHistoryRepository,
) {

    // The coroutine scope bound to the lifecycle of this service, used to store calculation history items in the DB using the IO Dispatcher
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    // We use an unlimited capacity for the channel buffer here, but please choose a sensible limit
    private val historyChannel = Channel<CalculationHistory>(capacity = UNLIMITED)

    @PostConstruct
    fun init() {
        // Launches a coroutine in our custom coroutine scope which receives items from the historyChannel
        coroutineScope.launch {
            // Looks like a traditional for-loop, but is actually suspending until an item is received on the historyChannel
            for (history in historyChannel) {
                logger.info { "Storing calculation in history: $history" }
                calculationHistoryRepository.save(history).awaitSingle()
            }
        }
    }

    @PreDestroy
    fun destroy() {
        // Stops the job that is launched in the init() function
        coroutineScope.cancel("Shutting down...")
    }

    // The `ReactiveCrudRepository.findAll()` function returns a reactive Flux, which we convert to a Kotlin coroutines-style Flow, using the `asFlow()` extension function
    fun findHistory(): Flow<CalculationHistory> =
        calculationHistoryRepository.findAll().asFlow()

    // The `ReactiveCrudRepository.findById(...)` function returns a reactive Mono, which we convert to a suspending 'regular' nullable return value, using the `awaitSingleOrNull()` extension function
    suspend fun findHistoryById(id: Long): CalculationHistory? =
        calculationHistoryRepository.findById(id).awaitSingleOrNull()

    suspend fun addToHistory(request: CalculationRequest, result: CalculationResult) {
        val history = CalculationHistory(
            operator = request.operator,
            operand1 = request.operand1,
            operand2 = request.operand2,
            result = result.result,
            isEven = result.isEven,
        )

        // Sending an item to the historyChannel will not suspend, as long as the channel has enough capacity
        // (which in our case is always true because we use an unlimited capacity).
        logger.info { "Queueing calculation to store in history: $history" }
        historyChannel.send(history)
    }

    companion object: KLogging()
}
