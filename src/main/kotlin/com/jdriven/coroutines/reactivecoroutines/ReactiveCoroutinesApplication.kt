package com.jdriven.coroutines.reactivecoroutines

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReactiveCoroutinesApplication

fun main(args: Array<String>) {
	runApplication<ReactiveCoroutinesApplication>(*args)
}
