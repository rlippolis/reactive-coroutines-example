package com.jdriven.coroutines.reactivecoroutines.domain

data class CalculationRequest(
    val operator: Operator,
    val operand1: Int,
    val operand2: Int,
)
