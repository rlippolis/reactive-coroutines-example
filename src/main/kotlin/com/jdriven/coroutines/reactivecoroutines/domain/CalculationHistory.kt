package com.jdriven.coroutines.reactivecoroutines.domain

import org.springframework.data.annotation.Id

data class CalculationHistory(
    @Id
    val id: Long? = null,
    val operator: Operator,
    val operand1: Int,
    val operand2: Int,
    val result: Int,
    val isEven: Boolean,
)
