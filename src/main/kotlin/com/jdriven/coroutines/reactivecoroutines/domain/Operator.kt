package com.jdriven.coroutines.reactivecoroutines.domain

enum class Operator {
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE
}
