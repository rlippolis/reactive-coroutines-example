package com.jdriven.coroutines.reactivecoroutines.domain

data class CalculationResult(
    val result: Int,
    val isEven: Boolean
)
