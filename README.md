# Reactive Kotlin Coroutines demo application

This application showcases the combination of a reactive Spring application using WebFlux and R2DBC with Kotlin Coroutines.
With this combination, it is possible to create a fully reactive non-blocking application, using imperative-style code with
structured concurrency. In other words: no need to work with Fluxes and Monos, but use suspending functions and coroutine Flows instead.

The application itself is functionally useless (basically a REST API that performs integer calculations, and indicates whether
the result of the calculation is odd or even), but it shows how easy it is to get started with creating a reactive Kotlin application.

## Architecture

The application uses Spring WebFlux for the REST API and client, and Spring R2DBC for the database access layer.
The database is a PostgreSQL instance, and the application depends on the [isEven REST API](https://isevenapi.xyz/) to
determine whether a number is even or not (because of course we need a webservice for that).

## How to run the application
                                
First, build the application using Gradle (optionally using the supplied Gradle wrapper) by running:

```shell
./gradlew build 
```

Then, use the provided `docker-compose.yml` file to start the application and database by running:
```shell
docker-compose up
```

## API endpoints

The REST API is exposed on `http://localhost:8080`. The following endpoints are available (only GET requests):

- `http://localhost:8080/calculation/{operator}/{operand1}/{operand2}`: Performs the requested calculation and returns the result.
  Supported calculations are: [`add`, `subtract`, `multiply`, `divide`]  

  Example request: `http://localhost:8080/calculation/add/3/6`

  Example response:
 ```json
 {"result":9,"isEven":false}  
 ```

- `http://localhost:8080/calculation/history`: Returns the history of performed calculations.
- `http://localhost:8080/calculation/history/{id}`: Returns a specific item from the history of performed calculations. 
